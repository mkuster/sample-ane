# Sample Adobe Native Extension for Android

## How to create IntelliJ Idea workspace:
1. Create empty project with location set to main folder

### AS3 library:
1. Create flash module with options: mobile, library, pure actionscript
2. Set module content root to "actionscript" folder

### Android library:
1. Create Android library module
2. Set module content root to "android" folder
3. Copy FlashRuntimeExtensions.jar (from AIR SDK) into "libs" folder and add it to dependencies

### Compile modules:
1. Menu->Build->Make project
2. If you have problems try to compile it with Java 1.6 version

### Generate ane:
1. Set path to $AIR_SDK/bin folder to be able to run adt command or do anything in $AIR_SDK/bin folder
2. create "android" and "default" folder
3. copy generated JAR file to "android" folder
4. copy generated SWC file to top folder ($AIR_SDK/bin or your top folder)
5. extract generated SWC file and copy library.swf file to "android" and "default" folder
6. copy extension.xml file to top folder ($AIR_SDK/bin or your top folder)
7. adt -package -target ane sample.ane extension.xml -swc sample.swc -platform Android-ARM -C android . -platform default -C default .
8. sample.ane file should be created

### Demo app:
1. Create flash module with options: mobile, application, pureactionscript
2. Set module content root to "demo" folder and "Main.as" as main class
3. Copy generated "sample.ane" file into "ane" folder and add it to dependencies
4. Copy generated SWC file into "libs" folder and add it to dependencies
5. Set APP descriptor file to "Main-app.xml" - it has added <extensionID>com.example.android.SampleExtension</extensionID> and added <uses-permission android:name="android.permission.VIBRATE"/>
6. Generate application Menu->Build->Package AIR Application
