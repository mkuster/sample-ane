package com.example.android
{
	import flash.events.Event;

	public class HelloWorldEvent extends Event
	{
		public static const HELLO:String = "com.example.android.HelloWorldEvent.HELLO";
		private var _message:String;

		public function HelloWorldEvent(message:String)
		{
			super(HELLO);
			_message = message;
		}

		public function get message():String
		{
			return _message;
		}
	}
}
