package com.example.android
{
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.StatusEvent;
	import flash.external.ExtensionContext;

	public class SampleExtension extends EventDispatcher
	{
		private static var _instance:SampleExtension;
		private static var _extensionContext:ExtensionContext = null;

		public function SampleExtension()
		{
			if(!_extensionContext)
			{
				createExtensionContext();
			}
			if(_extensionContext){
				_extensionContext.addEventListener(StatusEvent.STATUS, statusHandler);
			}
		}

		public static function getInstance():SampleExtension
		{
			if(!_instance)
			{
				_instance = new SampleExtension();
			}
			return _instance;
		}

		public function getDeviceName():String
		{
			_extensionContext.actionScriptData
			return String(_extensionContext.call("getDeviceName"));
		}

		public function vibrate(time:int):void
		{
			_extensionContext.call("vibrate", time);
		}

		public function getDelayedHelloWorld():void
		{
			_extensionContext.call("getDelayedHelloWorld");
		}

		private static function createExtensionContext():void
		{
			_extensionContext = ExtensionContext.createExtensionContext("com.example.android.SampleExtension", null);
		}

		private function statusHandler(event:StatusEvent):void
		{
			if(event.code == "hello") {
				dispatchEvent(new HelloWorldEvent(event.level));
			}
		}
	}
}
