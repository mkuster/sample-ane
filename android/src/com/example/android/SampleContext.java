package com.example.android;

import com.adobe.fre.FREContext;
import com.adobe.fre.FREFunction;
import com.example.android.functions.GetDelayedHelloWorld;
import com.example.android.functions.GetDeviceName;
import com.example.android.functions.Vibrate;

import java.util.HashMap;
import java.util.Map;

public class SampleContext extends FREContext {
	@Override
	public Map<String, FREFunction> getFunctions() {
		Map<String, FREFunction> functionsMap = new HashMap<String, FREFunction>();
		functionsMap.put("getDeviceName", new GetDeviceName());
		functionsMap.put("vibrate", new Vibrate());
		functionsMap.put("getDelayedHelloWorld", new GetDelayedHelloWorld());
		return functionsMap;
	}

	@Override
	public void dispose() {

	}
}
