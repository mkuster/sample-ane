package com.example.android.functions;

import com.adobe.fre.FREContext;
import com.adobe.fre.FREFunction;
import com.adobe.fre.FREObject;

public class GetDelayedHelloWorld implements FREFunction {
	@Override
	public FREObject call(FREContext freContext, FREObject[] freObjects) {
		final FREContext context = freContext;
		new android.os.Handler().postDelayed(
				new Runnable() {
					@Override
					public void run() {
						context.dispatchStatusEventAsync("hello", "hello world");
					}
				}, 1500
		);
		return null;
	}
}
