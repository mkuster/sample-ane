package com.example.android.functions;

import android.content.Context;
import android.os.Vibrator;
import com.adobe.fre.*;

public class Vibrate implements FREFunction {
	@Override
	public FREObject call(FREContext freContext, FREObject[] freObjects) {
		try {
			int time = freObjects[0].getAsInt();
			Context context = freContext.getActivity().getBaseContext();
			Vibrator vibrator = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
			vibrator.vibrate(time);
		} catch (FRETypeMismatchException e) {
			e.printStackTrace();
		} catch (FREInvalidObjectException e) {
			e.printStackTrace();
		} catch (FREWrongThreadException e) {
			e.printStackTrace();
		}
		return null;
	}
}
