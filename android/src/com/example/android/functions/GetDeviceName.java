package com.example.android.functions;

import android.os.Build;
import com.adobe.fre.FREContext;
import com.adobe.fre.FREFunction;
import com.adobe.fre.FREObject;
import com.adobe.fre.FREWrongThreadException;

public class GetDeviceName implements FREFunction {
	@Override
	public FREObject call(FREContext freContext, FREObject[] freObjects) {
		String deviceName = getDeviceName().toUpperCase();
		try {
			return FREObject.newObject(deviceName);
		} catch (FREWrongThreadException e) {
			e.printStackTrace();
		}
		return null;
	}

	private String getDeviceName() {
		String manufacturer = Build.MANUFACTURER;
		String model = Build.MODEL;
		if (model.startsWith(manufacturer)) {
			return model;
		}
		return manufacturer + " " + model;
	}
}
