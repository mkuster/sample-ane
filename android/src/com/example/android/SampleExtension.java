package com.example.android;

import com.adobe.fre.FREContext;
import com.adobe.fre.FREExtension;

public class SampleExtension implements FREExtension {
	@Override
	public void initialize() {

	}

	@Override
	public FREContext createContext(String s) {
		return new SampleContext();
	}

	@Override
	public void dispose() {

	}
}
