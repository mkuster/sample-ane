package
{
	import com.example.android.HelloWorldEvent;
	import com.example.android.SampleExtension;

	import flash.display.Sprite;
	import flash.display.StageAlign;
	import flash.display.StageScaleMode;
	import flash.events.MouseEvent;
	import flash.text.TextField;

	public class Main extends Sprite
	{
		private var _output:TextField;
		private var _deviceNameButton:Sprite;
		private var _vibrateButton:Sprite;
		private var _delayedHelloWorldButton:Sprite;

		public function Main()
		{
			stage.align = StageAlign.TOP_LEFT;
			stage.scaleMode = StageScaleMode.NO_SCALE;
			createOutput();
			createDeviceNameButton();
			createVibrateButton();
			createDelayedHelloWorldButton();
		}

		private function createOutput():void
		{
			_output = new TextField();
			_output.width = 300;
			_output.height = 300;
			addChild(_output);
		}

		private function createDeviceNameButton():void
		{
			_deviceNameButton = new Sprite();
			_deviceNameButton.graphics.beginFill(0xff00ff);
			_deviceNameButton.graphics.drawRect(0, 0, 50, 50);
			_deviceNameButton.graphics.endFill();
			_deviceNameButton.x = 50;
			_deviceNameButton.y = 350;
			_deviceNameButton.addEventListener(MouseEvent.CLICK, deviceNameButton_clickHandler);
			addChild(_deviceNameButton);
		}

		private function deviceNameButton_clickHandler(event:MouseEvent):void
		{
			_output.appendText(SampleExtension.getInstance().getDeviceName() + "\n");
		}

		private function createVibrateButton():void
		{
			_vibrateButton = new Sprite();
			_vibrateButton.graphics.beginFill(0xff00ff);
			_vibrateButton.graphics.drawRect(0, 0, 50, 50);
			_vibrateButton.graphics.endFill();
			_vibrateButton.x = 150;
			_vibrateButton.y = 350;
			_vibrateButton.addEventListener(MouseEvent.CLICK, deviceVibrate_clickHandler);
			addChild(_vibrateButton);
		}

		private function deviceVibrate_clickHandler(event:MouseEvent):void
		{
			SampleExtension.getInstance().vibrate(5000);
		}

		private function createDelayedHelloWorldButton():void
		{
			_delayedHelloWorldButton = new Sprite();
			_delayedHelloWorldButton.graphics.beginFill(0xff00ff);
			_delayedHelloWorldButton.graphics.drawRect(0, 0, 50, 50);
			_delayedHelloWorldButton.graphics.endFill();
			_delayedHelloWorldButton.x = 250;
			_delayedHelloWorldButton.y = 350;
			_delayedHelloWorldButton.addEventListener(MouseEvent.CLICK, delayedHelloWorldButton_clickHandler);
			addChild(_delayedHelloWorldButton);
		}

		private function delayedHelloWorldButton_clickHandler(event:MouseEvent):void
		{
			SampleExtension.getInstance().addEventListener(HelloWorldEvent.HELLO, helloHandler);
			SampleExtension.getInstance().getDelayedHelloWorld();
		}

		private function helloHandler(event:HelloWorldEvent):void
		{
			SampleExtension.getInstance().removeEventListener(HelloWorldEvent.HELLO, helloHandler);
			_output.appendText(event.message + "\n");
		}
	}
}
